﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class birds : MonoBehaviour
{

    public float speed = 0.2f;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(GetComponent<Renderer>().isVisible)
            transform.Translate(-speed * Time.deltaTime, 0f, 0f);
	}

}
